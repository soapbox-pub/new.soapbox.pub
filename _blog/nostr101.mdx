---
title: 'Nostr 101: A Beginner Guide to the Decentralized Social Network'
excerpt: How to join Nostr, how to create your account on Nostr, and what Nostr apps to use for beginners
coverImage: '/assets/blog/nostr-101.png'
date: '2025-02-16'
author:
  name: M. K. Fain
  picture: '/assets/avatars/mk.jpg'
ogImage: 
  url: '/assets/blog/nostr-101.png'
---

## What is Nostr?

**Nostr (Notes and Other Stuff Transmitted by Relays) is a decentralized, censorship-resistant social protocol designed to give users control over their online identities and interactions.** Unlike traditional social media platforms that rely on centralized servers and moderation, Nostr allows users to communicate directly through a network of independent relays. This makes it permissionless—meaning anyone can use it without needing approval from a corporation or government.

<iframe width='100%' height='400' src='https://www.youtube.com/embed/NVm_jGdwTjQ?si=5qcrxgjr4keqblo4' title='Nostr 101 Video' allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture' allowFullScreen></iframe>

### Key Features of Nostr:

- **Decentralized:** No single company controls Nostr; it operates on a distributed network of relays.
- **Censorship-Resistant:** Users cannot be banned from the network; they can always find a relay that will host their content.
- **Permissionless:** No accounts, phone numbers, or email verification required—just a cryptographic key pair.

## How to Create an Account on Nostr

Unlike traditional social media, Nostr does not require a username-password combo. Instead, it uses **cryptographic keys**:

- **Public Key (npub_...):** This is your identity, like a username. Anyone can see it and follow you.
- **Private Key (nsec_...):** This is your password. **Never share it!** It signs your posts and proves your identity.
- **Username (NIP05 domain):** NIP05 usernames allow users to have an easy-to-read, email-like handle (e.g., user@domain.com). You can pay for usernames from many services, or get one for free on [Ditto.pub](https://ditto.pub).


## Nostr Clients (Apps) vs. Relays

<figure style={{ width: '100%', height: 'auto' }}>
  <img alt='Best Nostr Apps' src='/assets/blog/best-nostr-apps.png' style={{ width: '100%', height: 'auto', objectFit: 'contain' }} />
</figure>

### Clients: How You Access Nostr
Clients are applications that allow users to read, write, and interact with content on Nostr. They function similarly to social media apps but with full decentralization.

There are multiple clients (apps) for accessing Nostr, each with different features. Because Nostr is decentralized, your account is not tied to any single app—you can take your private and public keys to any Nostr client, and your identity, followers, and posts will remain the same. This means you have one account, infinite apps to choose from. 

**Best Nostr Clients:**
- **Primal** (Web, iOS, Android) – Great for discovery and ease of use.
- **Damus** (iOS) – A clean, Apple-friendly experience.
- **Amethyst** (Android) – A popular choice for Android users.
- **Ditto** (Web, mobile web) – A simple and intuitive way to explore Nostr.
- Explore more clients on [Nostrapps.com](https://nostrapps.com).

Each client can connect to any relay, and users can switch between clients while keeping the same identity (public/private key pair).

### Relays: The Backbone of Nostr
Relays are servers that store and distribute messages between users. Unlike centralized platforms where a single entity owns the database, Nostr relays operate independently. Users can publish their messages to multiple relays to increase visibility and redundancy.

- Relays do not control user accounts; they only store and transmit messages.
- Anyone can run a relay, making the network decentralized.
- Some relays may charge a fee for posting messages to prevent spam.
- Users should connect to multiple relays to ensure their posts reach a wider audience.

### How Relays and Clients Work Together
1. A user posts a message via a client (e.g., Ditto or Primal).
2. The client signs the message with the user’s private key.
3. The message is sent to multiple relays.
4. Other users’ clients receive messages from relays they are connected to.
5. Users see updates in real-time, depending on which relays they follow.

This separation of data storage (relays) and user interfaces (clients) ensures that Nostr remains resilient, censorship-resistant, and user-controlled.

By understanding relays and clients, you can better navigate the Nostr ecosystem and make the most of its decentralized nature!


## Zaps: Tipping with Bitcoin

Zaps are a way to send small Bitcoin payments over the Lightning Network. Users can tip others for valuable content.

- **Get a Lightning Address:** Sign up with a service like Alby or Zebedee. You can also explore other Lightning address providers at [lightningaddress.com](https://lightningaddress.com).
- **Link Your Address to Your Nostr Profile:** Add it in your profile settings on your chosen client.
- **Start Receiving Zaps!** Users can now tip you directly from Nostr.

## How Nostr Differs from the Fediverse and Bluesky
Nostr is the only fully decentralized social network, but it shares similarities with the Fediverse and Bluesky. 

<figure style={{ width: '100%', height: 'auto' }}>
  <img alt='Comparison of Nostr, Fediverse, and Bluesky' src='/assets/blog/protocol-comparison.png' style={{ width: '100%', height: 'auto', objectFit: 'contain' }} />
</figure>

**Mostr Bridge: Connecting Nostr to the Fediverse**

The Mostr Bridge is a powerful tool that connects the Nostr network with the Fediverse (e.g., Mastodon, Pleroma). This allows users to follow and interact with accounts across both ecosystems without needing to switch platforms. Explore the bridge [here](https://mostr.pub).

## The Easiest Way to Get Started on Nostr

Nostr is a powerful and innovative platform that gives users full control over their online presence. If you want the easiest way to get started, [try Ditto.pub](https://ditto.pub) and start exploring the Nostr ecosystem today!

