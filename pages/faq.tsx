import React, { useState, useEffect } from 'react';

import Container from '../components/container';
import Layout from '../components/layout';
import PageTitle from '../components/page-title';
import UnifiedMeta from '../components/unified-meta';

const FAQ = () => {
    const [expandedSections, setExpandedSections] = useState({});
    const [copied, setCopied] = useState(null);

    useEffect(() => {
        const hash = window.location.hash.substring(1);
        if (hash) {
            setExpandedSections({ [hash]: true });
    
            // Delay to allow DOM updates before scrolling
            setTimeout(() => {
                const element = document.getElementById(hash);
                if (element) {
                    const offset = 100; // Lowers the page a bit to account for the fixed header
                    const elementPosition = element.getBoundingClientRect().top + window.scrollY;
                    window.scrollTo({
                        top: elementPosition - offset,
                        behavior: 'smooth',
                    });
                }
            }, 100);
        }
    }, []);
    

    const toggleSection = (section) => {
        setExpandedSections(prevState => ({
            ...prevState,
            [section]: !prevState[section],
        }));
    };

    const copyToClipboard = (id) => {
        const url = `${window.location.origin}${window.location.pathname}#${id}`;
        navigator.clipboard.writeText(url).then(() => {
            setCopied(id);
            setTimeout(() => setCopied(null), 2000);
        });
    };

    const questions = {
        'Ditto 101': [
            { q: 'What is Ditto?', a: 
                '<p>Ditto is social media software for the decentralized protocol Nostr. Designed to be self-hosted by for communities, it features a built-in Nostr relay,  custom theming, and community moderation. Ditto uses the front end of Soapbox, which has been the front end for millions of users across the decentralized web since 2019. <a href="https://soapbox.pub/ditto/" target="_blank">Learn more about Ditto</a>.</p>' },

            { q: 'What is Nostr?', a: 
                '<p>Nostr is a decentralized protocol that enables censorship-resistant communication. <a href="https://soapbox.pub/blog/nostr101/" target="_blank">Learn more about Nostr</a>.</p>' },

            { q: 'How is this different from Soapbox on the Fediverse?', a: 
                '<p>Ditto uses Nostr, while Soapbox has traditionally uses ActivityPub. However, Ditto still uses Mastodon API and the platforms are connected thanks to the <a href="https://soapbox.pub/blog/mostr-fediverse-nostr-bridge/" target="_blank">Mostr Bridge</a>! Learn more about the differences between various decentralized platforms <a href="https://soapbox.pub/blog/comparing-protocols/" target="_blank">here</a>.</p>' },

            { q: 'What makes Ditto different from other Nostr clients?', a: 
                '<p>While most Nostr clients prioritize individual agency, Ditto prioritizes community building. This is done by putting all users on a built-in community relay, plus other unique features! Learn more <a href="https://soapbox.pub/blog/announcing-ditto/" target="_blank">here</a>.</p>' },

            { q: 'What features does Ditto have?', a: 
                '<p>Ditto includes everything you need in a Nostr app: Advanced search and discover tools, media filters, moderation, integrated payments, and more! Try it for yourself on <a href="https://ditto.pub" target="_blank">Ditto.pub</a>.</p>' },

            { q: 'How can I find a Ditto community to join?', a: 
                '<p>Explore Ditto communities at <a href="https://soapbox.pub/servers/" target="_blank">Soapbox Servers</a>.</p>' },

            { q: 'Is Ditto Open Source?', a: 
                '<p>Yes! Find the source code on <a href="https://gitlab.com/soapbox-pub/ditto" target="_blank">GitLab</a>. We welcome contributors!</p>' },
        ],

        'Using Ditto': [
            { q: 'How do I log in to Ditto?', a: 
                '<p>Log in with your Nostr private key. You can do this either by directly entering your private key into login form, or using a Nostr signing tool like <a href="https://getalby.com/" target="_blank">Alby</a>. We never store your nsec in our database!</p>' },

            { q: 'How do I request a username?', a: 
                '<p>Submit a request in your account settings at Settings -> Identity. Once requested, your name will need to be manually approved. After your name is approved, don\'t forget to set it from your notifications!</p>' },

            { q: 'How do I add a lightning address?', a: 
                '<p>Lightning is a way to send small amounts of Bitcion (sats/zaps) to each other. To add your lightning address, click "Edit Profile" and then add your lighting address into the "Lightning Address" field. Don\'t forget to click save! If you don\'t have a lightning address yet, get one with any of <a href="https://lightningaddress.com/#providers" target="_blank">these providers</a>.</p>' },

            { q: 'How do I use Ditto on Android/iOS?', a: 
                '<p>Although Ditto does not have a dedicated mobile application, you can still access it seamlessly on your Android and iOS mobile devices by pinning the web app to your home screen. Learn more <a href="https://soapbox.pub/blog/how-to-install-ditto-mobile/" target="_blank">here</a>.</p>' },

            { q: 'Can I use other Nostr apps with my Ditto account?', a: 
                '<p>Yes! Ditto integrates with any microblogging Nostr client. Explore some Nostr apps <a href="https://nostrapps.com/#microblogging#all" target="_blank">here</a>.</p>' },

            { q: 'How do I submit an issue?', a: 
                '<p>Report issues on <a href="https://gitlab.com/soapbox-pub/ditto/issues" target="_blank">GitLab</a>.</p>' },
        ],

        'Hosting Ditto': [
            { q: 'Can Ditto be self-hosted?', a: 
                '<p>Yes! Install instructions are <a href="https://docs.soapbox.pub/ditto/install" target="_blank">here</a>.</p>' },

            { q: 'What technical skills do I need to self-host a Ditto community?', a: 
                '<p>At this stage in Ditto\'s development, we recommend some baseline technical knowledge before diving into Ditto including: comfort with the terminal on Linux (including installing pacakges, etc), familiarity with the basics of Nostr, ability to configure domain names, and a basic understanding of database management. However, we are working hard to make running Ditto more accesible to everyone in the future!</p>' },

            { q: 'What is the recommended hardware to run Ditto?', a: 
                '<p>This depends on a lot of factors, including your particular use case. However, in general we recommend 4 cores, 8 gb ram, and 100gb disk.</p>' },

            { q: 'How do I customize the theme of my Ditto community?', a: 
                '<p>Log in to your Ditto server as an admin, and go to Dashboard -> Soapbox Config -> Edit Theme.</p>' },

            { q: 'How do users join my new Ditto community?', a: 
            '<p>Once you have set up your Ditto server, share your instance url. From there, users can either create a new Nostr account or log in with their existing account. From there, they can request a NIP-05 username on your domain.</p>' },

            { q: 'How do I populate content in my new Ditto community?', a: 
                '<p>Add relays into your server firehose at Dashboard -> Relays. We recommend adding wss://relay.damus.io and wss://relay.primal.net to start populating your database. Please see <a href="https://docs.soapbox.pub/ditto/sync" target="_blank">this guide</a> on how to sync content.</p>' },

            { q: 'How do I moderate content on my Ditto community?', a: 
                '<p>Ditto provides built-in moderation tools, including the ability to mute users on your server. We also highly recommend configuring moderation policies with <a href="https://nostrify.dev/" target="_blank">Nostrify</a>.</p>' },

        ],
    };

    return (
        <Layout>
            <UnifiedMeta title='Ditto FAQ | Soapbox' />
            <Container>
                <PageTitle className='mt-16 text-center'>
                    Frequently Asked Questions
                </PageTitle>
                <div className='faq max-w-2xl mx-auto p-4'>
                
                    {Object.entries(questions).map(([section, qs]) => (
                        <div key={section} className='faq-section mb-16'>
                            <hr className='border-t border-gray-300 my-6' />
                            <h2 className='text-3xl text-azure font-semibold mb-4 pt-6'>{section}</h2>
                            {qs.map((item, index) => {
                                const id = `${section.replace(/\s+/g, '-').toLowerCase()}-${index}`;
                                return (
                                    <div key={index} className='faq-item mb-4' id={id}>
                                        <div
                                            onClick={() => toggleSection(id)}
                                            className='faq-question cursor-pointer p-4 bg-gray-100 hover:bg-gray-200 transition-colors flex justify-between items-center'
                                        >
                                            <span className='font-bold text-lg'>{item.q}</span>
                                            <button
                                                onClick={(e) => {
                                                    e.stopPropagation();
                                                    copyToClipboard(id);
                                                }}
                                                className='text-blue-600 hover:underline focus:outline-none'
                                            >
                                                {copied === id ? 'Copied!' : 'Copy link'}
                                            </button>
                                        </div>
                                        {expandedSections[id] && (
                                            <div className='faq-answer p-4'
    dangerouslySetInnerHTML={{
        __html: item.a.replace(
            /<a /g,
            '<a class="text-azure underline"',
        ),
    }}
/>
                                        )}
                                    </div>
                                );
                            })}
                        </div>
                    ))}
                </div>
            </Container>
        </Layout>
    );
};

export default FAQ;
