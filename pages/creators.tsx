import Link from 'next/link';
import React from 'react';

import Container from '../components/container';
import CreatorsForm from '../components/creators-form';
import Layout from '../components/layout';
import SimpleBanner from '../components/simple-banner';
import UnifiedMeta from '../components/unified-meta';

const sections = [
  {
    number: 1,
    heading: 'Personalize your Platform',
    subheading: 'We\'ll help you choose a URL, add custom branding, and configure your desired moderation settings',
    paragraph: '',
    image: null,
    color: 'bg-azure',
    ctaLink: '/blog/curated-communities-with-ditto/',
    ctaText: 'Learn about moderation on Ditto',
  },
  {
    number: 2,
    heading: 'Monetize Your Content with Bitcoin Lightning',
    subheading: 'We\'ll help you set up your lightning wallet and learn how to accept Zaps for your content',
    paragraph: null,
    image: null,
    color: 'bg-violet-500',
    ctaLink: '/blog/', //update
    ctaText: 'Learn more about setting up a lightning wallet',
  },
  {
    number: 3,
    heading: 'Launch Your Community on Nostr',
    subheading: 'Enjoy all the freedom of decentralized social media with the benefit of building an interest-based community',
    paragraph: '',
    image: null,
    color: 'bg-fuchsia-500',
    ctaLink: null,
    ctaText: null,
  },
];

const NumberedSections: React.FC = () => (
  <div className='space-y-24 mt-16'>
    {sections.map((section) => (
      <div key={section.number} className='flex flex-col md:flex-row space-x-12 max-sm:space-y-6 items-center md:content-start md:items-start justify-center md:justify-start'>
        <div className='flex-shrink-0'>
          <div className={`h-48 w-48 rounded-full ${section.color} flex items-center justify-center`}>
            <span className='text-white text-9xl font-bold'>{section.number}</span>
          </div>
        </div>
        <div className='items-center justify-center md:justify-start text-center md:text-left space-y-3 mt-3 md:mt-0'>
          <h2 className='text-4xl font-bold'>{section.heading}</h2>
          <h3 className='text-xl text-gray-600'>{section.subheading}</h3>
          {section.paragraph && <p className='mt-2 text-gray-700'>{section.paragraph}</p>}
          {section.image && <img src={section.image} alt={`Graphic for section ${section.number}`} className='mt-4' />}
          {section.ctaText && <p className='text-xl font-semibold text-azure hover:text-violet-500'>
            <a href={section.ctaLink}>{section.ctaText} →</a>
          </p>}
        </div>
      </div>
    ))}
  </div>
);

export default function CreatorsPage() {
  return (
    <Layout>
      <UnifiedMeta 
        title='Creators | Soapbox' 
        description='Join the Ditto Creator Incubator and get customized support with cross-platform promotion.'
        image='/assets/images/creators-og.png'
      />
      <SimpleBanner 
        heading='Build Your Community.'
        subheadings={[
          'Full control over your data.',
          'Censorship-resistant platform.',
          'Monetize your content with Bitcoin.',
        ]}
        backgroundColorClassName='bg-azure'
        imageUrl='/assets/images/creators/creators.png'
      />
      <div className="bg-repeat-y bg-contain md:bg-[url('/assets/images/creators/twinkle.gif')] pb-40">

        <h1 className='text-azure text-6xl lg:text-7xl font-bold leading-snug text-center mt-16'>
          Join The Ditto Creator&nbsp;Incubator
        </h1>

        <Container className='max-w-4xl'>
          <div className='flex-col text-center place-content-center space-y-6 py-16 justify-items-center justify-around'>
            
            <h2 className='text-2xl md:text-3xl font-semibold'>
              Get customized support with cross-platform promotion
            </h2>
            <p className='text-lg'>
              The Ditto Creator Incubator empowers emerging creators by providing them with resources, support, 
              and exposure to build and monetize their social community on Nostr. The program offers creators a unique opportunity to 
              focus on their content without the constraints of traditional social media algorithms and censorship. 
              Participants receive personalized onboarding, 
              support for setting up Bitcoin micropayments with lightning, and promotion across multiple channels. 
              <span className='font-semibold text-azure hover:text-violet-500'><Link href='/ditto'>Explore&nbsp;Ditto&nbsp;→</Link></span>
            </p>
          </div>

          <NumberedSections />

          <div className='relative flex py-8 items-center'>
            <div className='flex-grow border-t border-gray-200 my-12' ></div>
          </div>

          <div>
            <CreatorsForm />
          </div>

        </Container>

      </div>
    </Layout>
  );
}
