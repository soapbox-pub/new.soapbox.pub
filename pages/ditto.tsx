import { IconArrowRight } from '@tabler/icons';
import Image from 'next/image';

import Button from '../components/button';
import Container from '../components/container';
import { Features, Feature } from '../components/feature';
import Layout from '../components/layout';
import SimpleBanner from '../components/simple-banner';
import TeamMember from '../components/team-member';
import UnifiedMeta from '../components/unified-meta';


export default function DittoPage() {
  return (
    <Layout>
      <UnifiedMeta title='Ditto | Soapbox' image='/assets/images/ditto/ditto-og.png' />

      <SimpleBanner 
          heading='Ditto 🪐'
          subheadings={[
            'Build Your Community on Nostr',
          ]}
          paragraph='Ditto is a decentralized, self-hosted social media server that emphasizes user control and community building across platforms. Key features include a built-in Nostr relay, compatibility with any Mastodon app, and full customizability. Always open-source, no ads, no tracking, and no censorship. '
          backgroundColorClassName='bg-azure'
          imageUrl='/assets/images/ditto/ditto-phones.png'
          ctaText='Try it today on Ditto.pub'
          ctaUrl='https://ditto.pub'
        />

      <Container className='my-24'>
      <div className='my-8'>
        <Features>
            
            <Feature
              span={2}
              title={<>Decentralized, <span className='whitespace-nowrap'>Censorship-resistant,</span> <span className='font-bold'>Communities on Nostr</span>.</>}
              blurb='Find your tribe. Build your audience. Monetize your content.
              Key features include a built-in Nostr relay, NIP-05 self-service, compatibility with any Mastodon app, 
              and full customizability. Always open-source, no ads, no tracking, and no censorship.'
              direction='horizontal'
              ctaText='Learn More'
              ctaLink='https://soapbox.pub/blog/curated-communities-with-ditto/'
            >
              <div className='relative'>
                <Image
                  className='h-full object-cover object-left-top'
                  src='/assets/images/ditto/community-users.png'
                  width='1250'
                  height='750'
                  alt='Ditto phones'
                  unoptimized
                />
              </div>
            </Feature>

            <Feature
              title={<><span className='font-bold'>Customizable Themes</span></>}
              blurb='Custom color schemes and logos help make your community stand out
            and work seamlessly across both light and dark themes.'
              ctaText=''
              ctaLink=''
            >
              <div className='relative place-self-center'>
                <Image
                  className=''
                  src='/assets/images/ditto/theme-editor.png'
                  width='450'
                  height='280'
                  alt='Themes'
                  unoptimized
                />
                <div className='absolute w-full h-1/3 z-10 inset-x-0 bottom-0 bg-gradient-to-t from-white' />
              </div>
            </Feature>

            <Feature
              title={<>Monetize with <span className='font-bold whitespace-nowrap'>Bitcoin Lightning Zaps</span>.</>}
              blurb='Clicking the Zap button on a post will send a small amount of Bitcoin (aka sats) 
              to the author of the post. It means anyone in the world, including you, can get paid just for posting online.'
            >
              <div className='relative place-self-center'>
                <Image
                  className=''
                  src='/assets/images/ditto/zaps3.png'
                  width='450'
                  height='280'
                  alt='Lightning'
                  unoptimized
                />
              </div>
            </Feature>

            <Feature
              title={<>Advanced <span className='font-bold whitespace-nowrap'>Search Features</span></>}
              blurb='Discover video feeds, find trending topics, or even filter by language.'
              ctaText='Try it'
              ctaLink='https://nostrify.dev/'
            >
              <div className='relative place-self-center'>
                <Image
                  className=''
                  src='/assets/images/ditto/discover2.png'
                  width='450'
                  height='280'
                  alt='Search'
                  unoptimized
                />
              </div>
            </Feature>

            <Feature
              title={<><span className='font-bold whitespace-nowrap'>Bridging</span> the Decentralized Web.</>}
              blurb='Only Ditto supports both Nostr and ActivityPub, so your posts can reach across the Fediverse and beyond!'
              ctaText='Explore the Mostr Bridge'
              ctaLink='https://mostr.pub/'
            >
              <div className='relative place-self-center'>
                <Image
                  className=''
                  src='/assets/blog/mostr-banner.png'
                  width='450'
                  height='280'
                  alt='Mostr'
                  unoptimized
                />
              </div>
            </Feature>

            <Feature
              span={2}
              title={<>Latest Release: <span className='font-bold'>Ditto 1.2</span></>}
              blurb='We are thrilled to announce the release of Ditto 1.2, packed with enhancements designed to make Ditto communities more user-friendly and engaging.'
              direction='horizontal'
              ctaText='Read More'
              ctaLink='/blog/ditto-1.2'
            >
              <div className='relative'>
                <Image
                  className='h-full object-cover object-left-top'
                  src='/assets/blog/ditto-1.2/ditto-1.2.png'
                  width='1100'
                  height='620'
                  alt='Ditto 1.2 Blog'
                  unoptimized
                />
              </div>
            </Feature>

            <Feature
              span={1}
              direction='vertical'
              title={<><span className='font-bold whitespace-nowrap'>Creator Incubator</span></>}
              blurb=' The Ditto Creator Incubator empowers emerging creators by providing them with resources, support, 
              and exposure to build and monetize their social community on Nostr. '
              ctaText='Learn more and apply'
              ctaLink='/creators'
            >
              <div className='relative place-self-center'>
                <Image
                  className=''
                  src='/assets/images/ditto/creators.png'
                  width='450'
                  height='280'
                  alt='Creators'
                  unoptimized
                />

                <div className='absolute w-full h-1/3 z-10 inset-x-0 bottom-0 bg-gradient-to-t from-white' />
              </div>
            </Feature>

            <Feature
              span={1}
              className='min-h-[400px] text-white'
              title={<span><div className='mt-40 pt-10'></div><span className='font-bold'>Cobrafuma.com</span></span>}
              blurb='Cobrafuma é uma comunidade para brasileiros no Nostr.'
            >

              <div className='px-6 pb-6'>
                  <Button theme='outline' href='https://cobrafuma.com'>Sign Up →</Button>
              </div>

              <Image
                  className='absolute top-0 mx-3'
                  src='/assets/images/ditto/cobrafuma-padded.png'
                  width='200'
                  height='250'
                  alt='Cobrafuma'
                  unoptimized
                />

              <div className='absolute h-full w-full -z-10 inset-0 bg-gradient-to-r to-green-500 from-green-700 text-white' />
            </Feature>

            <Feature
              span={1}
              className='min-h-[400px] text-white'
              title={<span><div className='mt-40 pt-10'></div><span className='font-bold'>Henhouse.social</span></span>}
              blurb='The first Nostr community for women, built on Ditto.'
            >

              <div className='px-6 pb-6'>
                  <Button theme='outline' href='https://henhouse.social'>Sign Up →</Button>
              </div>

              <Image
                  className='absolute top-0 mx-3'
                  src='/assets/images/henhouse-logo-white.png'
                  width='200'
                  height='250'
                  alt='Henhouse'
                  unoptimized
                />

              <div className='absolute h-full w-full -z-10 inset-0 bg-gradient-to-r from-orange-500 to-violet-500 text-white' />
            </Feature>

            <Feature
              span={1}
              className='min-h-[400px] text-white'
              title={<span><div className='mt-40 pt-10'></div><span className='font-bold'>Ditto.pub</span></span>}
              blurb='The flagship Ditto community!.'
            >

              <div className='px-6 pb-6'>
                  <Button theme='outline' href='https://Ditto.pub'>Sign Up →</Button>
              </div>

              <Image
                  className='absolute top-0 mx-3'
                  src='/assets/images/ditto/ditto-logo-transparent.png'
                  width='200'
                  height='250'
                  alt='Ditto'
                  unoptimized
                />

              <div className='absolute h-full w-full -z-10 inset-0 bg-gradient-to-r to-violet-700 from-azure text-white' />
            </Feature>

          </Features>
        </div>

      </Container>

      <Container className='flex justify-center my-48'>
        <div className='flex flex-col items-center space-y-8 text-center rounded-full bg-white shadow-white shadow-[0_20px_50px_50px]'>
          <h2 className='text-3xl lg:text-5xl font-semibold leading-none'>
            Related Projects
          </h2>
          <h2 className='text-xl font-semibold mt-6 mb-24'>
            Building an flexible ecosystem with tools for use across platforms
          </h2>

          <div className='grid lg:grid-cols-3 gap-20'>
            <a href='https://soapbox.pub/blog/mostr-fediverse-nostr-bridge/' target='_blank'>
              <TeamMember
                name='Mostr Bridge'
                avatar='/assets/images/ditto-cartoon-planet.png'
                bio='Mostr is a bridge between Nostr and the Fediverse (Mastodon, ActivityPub, etc). It allows users on both networks to communicate, through a Mostr server.'
              />
            </a>
            

            <a href='https://nostrify.dev/' target='_blank'>
              <TeamMember
                name='Nostrify'
                avatar='/assets/images/ditto-cartoon-ufo.png'
                bio='Bring your projects to life on Nostr with this flexible and pure Typscript framework on Deno and web.'
              />
            </a>
            

            <a href='https://gitlab.com/soapbox-pub/strfry-policies' target='_blank'>
              <TeamMember
                name='Strfry Policies'
                avatar='/assets/images/ditto-cartoon-telescope.png'
                bio='A collection of policies for the strfry Nostr relay, built in Deno.'
              />
            </a>
            
          </div>
        </div>
      </Container>

      <div className='bg-gradient-to-r from-indigo-500 to-blue-500 text-white py-20'>
        <Container>
          <div className='max-w-prose space-y-6'>
            <h2 className='text-4xl font-semibold'>Get Involved</h2>
            <p className='text-xl'>Soapbox is seeking active contributors to join our contributor community. Browse open issues on GitLab to begin contributing today!</p>
            <div className='space-x-4'>
            <Button theme='secondary' href='https://gitlab.com/soapbox-pub/ditto' group>
                <span className='flex items-center space-x-2 group'>
                  <span>Contribute</span>
                  <IconArrowRight className='transition-all w-5 group-hover:translate-x-1' />
                </span>
              </Button>
            </div>
            <p>Looking for work? We're also looking for core contributors open to contract. <br />Get in touch if that could be you! <a href='soapbox.pub/mentorships</p>'><span className='font-bold underline'>Learn More.</span></a></p>
          </div>
        </Container>
      </div>

    </Layout>
  );
}