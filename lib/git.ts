import { execFileSync } from 'child_process';

const getLastModified = (filename: string): string => {
  const raw = execFileSync('git', ['log', '-1', '--pretty=%cI', filename]);
  return String(raw).trim();
};

export {
  getLastModified,
};