import { IconExternalLink } from '@tabler/icons';

import Avatar from './avatar';

interface ITeamMember {
  name: string;
  avatar: string;
  bio: string;
  url?: string;
  cta?: string;
}

const TeamMember: React.FC<ITeamMember> = ({ name, avatar, bio, url, cta }) => {
  return (
    <div className='flex flex-col gap-6 items-center'>
      <Avatar src={avatar} size={256} alt={name}/>
      <h3 className='text-2xl font-semibold w-full text-center'>{name}</h3>
      <p className='w-full text-left'>{bio}</p>
      {cta && url && (
        <a
          href={url}
          className='flex space-x-1 items-center font-bold text-azure w-full text-left'
          target='_blank'
          rel='noopener noreferrer'
        >
          <span>{cta}</span>
          <IconExternalLink height={20} />
        </a>
      )}
    </div>
  );
};

export default TeamMember;
