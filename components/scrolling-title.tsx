import { useEffect, useState } from 'react';

type Option = {
  text: string;
  color: string;
};

type Props = {
  title: string;
  options: Array<Option>;
};

const ScrollingTitle = ({ title, options }: Props) => {
  const [optionIndex, setOptionIndex] = useState(0);

  useEffect(() => {
    const tid = setTimeout(() => {
      setOptionIndex((optionIndex + 1) % options.length);
    }, 2500);

    return () => {
      clearTimeout(tid);
    };
  }, [optionIndex]);

  return (
    <div className='h-32 md:h-72'>
      <h1 className='my-16 sm:my-24 md:my-36 text-3xl sm:text-6xl md:text-7xl text-center font-extrabold leading-tight md:leading-none'>
        <span>{title}</span>
        <br />
        <div className={`mt-4 ${options[optionIndex].color}`}>
          {options[optionIndex].text}
        </div>
      </h1>
    </div>
  );
};

export default ScrollingTitle;
