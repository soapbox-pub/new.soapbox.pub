import { useMDXComponent } from 'next-contentlayer/hooks';

import mdxComponents from '../../lib/mdx-components';
import Prose from '../prose';
import Wrapper from '../wrapper';

type Props = {
  content: string
}

const PostBody = ({ content }: Props) => {
  const MDXContent = useMDXComponent(content);

  return (
    <Wrapper>
      <Prose>
        <MDXContent components={mdxComponents} />
      </Prose>
    </Wrapper>
  );
};

export default PostBody;
