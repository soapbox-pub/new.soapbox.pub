import clsx from 'clsx';

type Props = {
  className?: string
  children?: React.ReactNode
}

const Container = ({ className, children }: Props) => {
  return (
    <div className={clsx(className, 'container mx-auto px-5')}>
      {children}
    </div>
  );
};

export default Container;
