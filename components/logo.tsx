import Image from 'next/image';
import React from 'react';

import soapboxLogo from '../public/assets/logo.svg';

interface ILogo {
  withText?: boolean
}

const Logo: React.FC<ILogo> = ({ withText = false }) => {
  return (
    <div className='flex justify-center items-center space-x-2'>
      <Image
        alt='Logo'
        src={soapboxLogo}
        className='w-6 h-6 md:w-8 md:h-8'
      />

      {withText && (
        <span className='text-xl md:text-2xl font-semibold'>
          Soapbox
        </span>
      )}
    </div>
  );
};

export default Logo;